This is an minimal implementation of a twitter post publishing.

Setting up:

* Put all the files inside a folder in your wp-contents/plugins folder. For the sake of simplicity, we're assuming you're putting it in the 'wp-contents/plugins/utweet' folder, but any name should do.
* Go to the twitter applications page (https://dev.twitter.com/) and create one for yourself.
* Set the callback page to your website. For our example, we'll assume that the site address is 'http://example.com'
** Remember to set your application to read-write, or you won't be able to publish anything.
** Make note of your Consumer Key and Consumer Secret. You'll net to put them on your wordpress.
* Go to wordpress, activate the plugin and go to the uTweet configuration page
* Put your consumer key and secret and push the Update button
** If the consumer key/secret is wrong, you'll get an warning and won't be able to get past this point. Get those values right (watch for spaces after or before the text).
* If all went well, you'll be presented with a 'Request authorization' link. Press that link to be redirected to twitter page for authorization
* Once you're back, you should see your screen name. Now you're ready to tweet your posts. Check for a "Publish to twitter" link near the "Publish" button when editing/creating posts.

Enjoy!
- Sergio Moura