<?php
/*
Plugin Name: uTweet
Plugin URI: http://gitorious.org/utweet
Description: Post your articles to twitter
Version: 0.2
Author: Sergio Moura
Author URI: http://icetempest.com
License: GPLv3
*/
define('UTWEET_VERSION', '0.2');
define('UTWEET_PLUGIN_URL', 'http://icetempest.com/');
//define('UTWEET_DEBUG', 1);

// The name of the database field to store our options.
define('UTWEET_OPTIONS_FIELD', 'utweet_options');
define('UTWEET_NOTICE_FIELD', 'utweet_notice');
define('UTWEET_PUBLISHEDAT_FIELD', 'utweet_published_at');
define('UTWEET_SHORTENED_LINK', 'utweet_shortened_link');

// No direct-access
if (!function_exists('add_action')) {
	echo("Sorry, I need Wordpress to work.");
	exit;
}

require_once(dirname(__FILE__) . "/uoauth/uOAuth.php");

if (is_admin()) {
	add_action('init', 'utweet_init');
	add_action('admin_menu', 'utweet_config_page');
}

$post_types = get_post_types(array('exclude_from_search' => false), 'objects');

foreach ($post_types as $post_type) {
	add_action('publish_' . $post_type->name, 'utweet_publish_action');
}

add_action('post_submitbox_start', 'utweet_post_button');
add_action('admin_notices', 'utweet_admin_notice');

//update_option(UTWEET_NOTICE_FIELD, "Howdy!");

function utweet_admin_notice(){
	$notice = get_option(UTWEET_NOTICE_FIELD, '');
	if ($notice != '') {
		echo '<div class="updated"><p>uTweet: ' . $notice . '</p></div>';
	}
	update_option(UTWEET_NOTICE_FIELD, '');
}

// Register action for each post type

function utweet_init() {
	global $utweet_options;
	
	$options = get_option(UTWEET_OPTIONS_FIELD, 'a:0:{}');
	
	$utweet_options = unserialize($options);
}

function utweet_config_page() {
	if (function_exists('add_submenu_page')) {
		add_submenu_page('plugins.php', __('uTweet Configuration'), __('uTweet Configuration'), 'manage_options', 'utweet-config', 'utweet_conf');
	}
}

// Shorten URL using BitLy
// Code adapted from http://davidwalsh.name/bitly-php (accessed at 2011-12-13) and http://dtio.net/2010/08/15/using-bit-ly-php-api/ (accessed at 2011-12-13)
function __utweet_shortenurl_bitly($url, $login, $apikey, $format = 'json', $version = '2.0.1') {
	$query = array(
		'version' => $version,
		'longUrl' => $url,
		'login' => $login,
		'apiKey' => $apikey,
		'format' => $format
		);
		
	$query = http_build_query($query);
	
	// create url
	$requesturl = 'http://api.bit.ly/shorten?' . $query;
	
	$response = file_get_contents($requesturl);
	
	if (strtolower($format) == 'json') {
		// json
		$json = @json_decode($response, true);
		return($json['results'][$url]['shortUrl']);
	}
	else {
		// xml
		$xml = simplexml_load_string($response);
		return('http://bit.ly/' . $xml->results->nodeKeyVal->hash);
	}
}

function __utweet_save_options() {
	global $utweet_options;
	update_option(UTWEET_OPTIONS_FIELD, serialize($utweet_options));
}

function __utweet_update_config() {
	if (function_exists('current_user_can') && !current_user_can('manage_options')) {
		die("Someone is cheating...");
	}
	
	global $utweet_options;
	
	if (array_key_exists('consumer_key', $_POST)) {
		$utweet_options['consumer_key'] = $_POST['consumer_key'];
		$utweet_options['consumer_secret'] = $_POST['consumer_secret'];

		__utweet_request_keys();
	}
	
	// Parse our non-twitter related parameters
	$otherparams = array('shorten', 'bitly_login', 'bitly_apikey');
	foreach($otherparams as $p) {
		if (array_key_exists($p, $_POST))
			$utweet_options[$p] = $_POST[$p];
	}
	
	__utweet_save_options();
}

function __utweet_post_tweet($tweet) {
	global $utweet_options;

	$oa = new uOAuth();
	$oa->consumerKey = $utweet_options['consumer_key'];
	$oa->consumerSecret = $utweet_options['consumer_secret'];
	$oa->token = $utweet_options['oauth_token'];
	$oa->tokenSecret = $utweet_options['oauth_token_secret'];

	$params = array();
	$params['status'] = $tweet;

	$x = $oa->request('POST', 'http://api.twitter.com/1/statuses/update.json', $params);
	//$x = "kudos!";
	return(json_decode($x, true));
}

function __utweet_request_keys() {
	global $utweet_options;
	global $utweet_message;

	$oa = new uOAuth();
	$oa->oauth_callback = admin_url() . '/plugins.php?page=utweet-config';
	$oa->consumerKey = $utweet_options['consumer_key'];
	$oa->consumerSecret = $utweet_options['consumer_secret'];
		
	$x = $oa->request('POST', 'https://api.twitter.com/oauth/request_token');
	$param_list = explode('&', $x);
	$params = array();
	foreach ($param_list as $p) {
		$k = explode('=', $p);
		$params[$k[0]] = $k[1];
	}

	if (!isset($params['oauth_token']) || $params['oauth_token'] == '') {
		$utweet_message = $x;
	}
		
	$utweet_options['oauth_token'] = $params['oauth_token'];
	$utweet_options['oauth_token_secret'] = $params['oauth_token_secret'];
}

function __utweet_verify_info() {
	global $utweet_options;
	global $utweet_message;

	$oa = new uOAuth();
	$oa->consumerKey = $utweet_options['consumer_key'];
	$oa->consumerSecret = $utweet_options['consumer_secret'];
	$oa->token = $_GET['oauth_token'];
	$oa->tokenSecret = $utweet_options['oauth_token_secret'];
	$oa->oauth_verifier = $_GET['oauth_verifier'];

	$x = $oa->request('POST', 'https://api.twitter.com/oauth/access_token');
	
	$param_list = explode('&', $x);
	$params = array();
	foreach ($param_list as $p) {
		$k = explode('=', $p);
		$params[$k[0]] = $k[1];
	}

	if (!isset($params['screen_name']) || $params['screen_name'] == '') {
		$utweet_message = $x;
		unset($utweet_options['oauth_token']);
		unset($utweet_options['oauth_token_secret']);
	}
	else {
		$utweet_options['oauth_token'] = $params['oauth_token'];
		$utweet_options['oauth_token_secret'] = $params['oauth_token_secret'];
		$utweet_options['user_id'] = $params['user_id'];
		$utweet_options['screen_name'] = $params['screen_name'];
	}
	
	__utweet_save_options();
}

/* Displays our form for data input or, if the data is already validated, just display the information */
function __utweet_consumer_form() {
	global $utweet_options;
	$post_url = admin_url() . 'plugins.php?page=utweet-config';

	echo('<div id="utweet_options">');
	?>
	<form action="<?php echo($post_url); ?>" method="post" id="utweet-conf" style="">
	<?php

	// If the data is good, 'oauth_token' should be set. If not, ask for the data
	if (!isset($utweet_options['oauth_token'])):
	?>
			<div>Consumer key: <input type="text" name="consumer_key" size="40" value="<? echo($utweet_options['consumer_key']); ?>" /></div>
			<div>Consumer secret: <input type="text" name="consumer_secret" size="60" value="<? echo($utweet_options['consumer_secret']); ?>" /></div>
	<?php
	else:
		if (!array_key_exists('shorten', $utweet_options)) {
			$shorten = "no";
		}
		else {
			$shorten = $utweet_options['shorten'];
		}
	?>
		<div>Consumer key: <? echo($utweet_options['consumer_key']); ?></div>
		<div>Consumer secret: <? echo($utweet_options['consumer_secret']); ?></div>
		
		<div>
			<h4>Shorten URL</h4>
			<input type="radio" name="shorten" value="no" <?php if ($shorten == 'no') echo "checked "; ?>/>Do not shorten URLs<br />
			<input type="radio" name="shorten" value="bitly" <?php if ($shorten == 'bitly') echo "checked "; ?>/>Shorten using bit.ly
			
			<h5>Bit.ly options</h5>
			<div>Login: <input type="text" name="bitly_login" size="40" value="<? echo($utweet_options['bitly_login']); ?>" /></div>
			<div>API Key: <input type="text" name="bitly_apikey" size="60" value="<? echo($utweet_options['bitly_apikey']); ?>" /></div>
			<div><input type="submit" name="submit" value="Update" /></div>
		</div>

	<?php
	endif;
	echo('</form></div>');
}

/* Sets up the plugin configuration page */
function utweet_conf() {
	global $utweet_options;
	global $utweet_message;

	$utweet_message = '';
	
	// Update our configuration and request the authorization token
	if (isset($_POST['submit'])) {
		__utweet_update_config();
	}

	// Twitter just redirected us with user information
	if (isset($_GET['oauth_verifier'])) {
		__utweet_verify_info();
	}
	
	// User is performing some action
	if (isset($_GET['action'])) {
		// Reset ALL the data
		if ($_GET['action'] == 'reset') {
			$utweet_options = array();
			__utweet_save_options();
		}
	}

	echo('<div id="utweet-header"><h3>uTweet configuration</h3></div>');
	
	// Displays message if any of our previous functions have returned anything
	if ($utweet_message != '') {
		echo('<div class="utweet_msg" style="width: 80%; text-align: center; border: 1px solid red; background: #fdd;">' . $utweet_message . '</div>');
	}

	// Display our screen name, if set.
	if (isset($utweet_options['screen_name'])) {
		echo("<div><h4>Screen name: @" . $utweet_options['screen_name'] . "</h4></div>");
	}

	__utweet_consumer_form();

	// Request authorization link (goes to twitter for account authorization)
	if (!isset($utweet_options['screen_name']) && isset($utweet_options['oauth_token'])):
	?>
		<div>
			<a href="http://api.twitter.com/oauth/authorize?oauth_token=<?php echo($utweet_options['oauth_token']); ?>">Request authorization</a>
		</div>
	<?php 
	endif;

	// Provides the user a functionality to reset everything and start over			
	if (isset($utweet_options['consumer_key'])):
	?>
		<div>
			<a href="<?php echo(admin_url() . 'plugins.php?page=utweet-config&action=reset'); ?>">Reset data</a>
		</div>
	<?php
	endif;

	if (defined('UTWEET_DEBUG')) {
		echo('<div id="debug"><pre>' . print_r($utweet_options, true) . '</pre></div>');
	}
}

/**
 * Renders a 'publish to twitter' checkbox. Renders the box only if 
 * the current post is a real post, not a page or something else.
 */
function utweet_post_button() {
	global $post;
	global $utweet_options;
	
	if (!is_object($post) || !current_user_can('publish_posts')) {
		return;
	}
	
	if ($post->post_status == "private") {
		echo '<div><em style="color:#aa6600">'.__('Private posts are not publishable').'</em></div>';
	}
	else {
		$disabled = '';
		$checked = 'checked="checked"';
		$publish_text = __('Publish to Twitter account');

		$published_at = get_post_meta($post->ID, UTWEET_PUBLISHEDAT_FIELD, true);

		if (!isset($utweet_options['screen_name'])) {
			$disabled = 'disabled="disabled"';
		}

		if ($published_at != '') {
			$checked = '';
			$publish_text = __('Publish to Twitter again');
			$publish_text = '<a href="#" title="' . __('Last published at') . ": " . $published_at . '">' . $publish_text . '</a>';
		}

		//$publish_text .= print_r($post, true);
		
		echo '<label for="utweet_post_check">' . $publish_text . '</label><input type="checkbox" name="utweet_post_check" id="utweet_post_check" $checked $disabled />';
	}
}

function utweet_publish_action($post_id) {
	global $utweet_options;

	if (!isset($utweet_options['screen_name'])) {
		return;
	}

	$post = get_post($post_id);

	if (!isset($_REQUEST['utweet_post_check']) || $_REQUEST['utweet_post_check'] != 'on') {
		//update_option(UTWEET_NOTICE_FIELD, "Not posting tweet: " . $_REQUEST['utweet_post_check'] . "<pre>" . print_r($_REQUEST, true) . "<pre>\n");
		return;
	}
	
	$url = get_permalink($post->ID);
	if (array_key_exists('shorten', $utweet_options)) {
		if ($utweet_options['shorten'] == 'bitly') {
			$newurl = __utweet_shortenurl_bitly($url, $utweet_options['bitly_login'], $utweet_options['bitly_apikey']);
			if ($newurl != '') {
				$url = $newurl;
				update_post_meta($post->ID, UTWEET_SHORTENED_LINK, $url);
			}
		}
	}
	
	$tweet = 'New post: ' . $post->post_title . '. ' . $url;

	// Updates the meta of the post, telling it when we last published this post to twitter
	$data = __utweet_post_tweet($tweet);
	if (isset($data['error'])) {
		update_option(UTWEET_NOTICE_FIELD, "Error tweeting message: '$tweet'<br />\n" . $data['error']);
	}
	else {
		update_post_meta($post->ID, UTWEET_PUBLISHEDAT_FIELD, date(DATE_ATOM));
		update_option(UTWEET_NOTICE_FIELD, "Message tweeted: '$tweet'" . "<br />\n");
	}
}

